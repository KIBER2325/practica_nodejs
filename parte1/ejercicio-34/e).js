const clientes = [
  {
  id: 1, nombre: 'Leonor'},
  {
  id: 2, nombre: 'Jacinto'},
  {
  id: 3, nombre: 'Waldo'}
]
const pagos = [
  {
      id: 1, pago: 1000, moneda: 'Bs'},
  {
      id: 2, pago: '1800', moneda: 'Bs'}
]

const id = 1;

const getCliente = (id) => {

  return new Promise((resolve, reject) => {
    const cliente = clientes.find(e => e.id === id);
    if(cliente){
      resolve(cliente);
    } else {
      reject(`No existe el cliente con el id ${id}`);
    }
  });
};

const getPago = (id) => {

  return new Promise((resolve, reject) => {
    const pago = pagos.find(s => s.id === id);
    if(pago){
      resolve(pago);
    } else {
      reject(`No existe el pago con el id ${id}`);
    }
  });
}
const getInfoUsuario = async (id) => {
  try {
    const cliente = await getEmpleado(id);
    const pago = await getSalario(id);
    return `El pago del empleado: ${cliente.nombre} es de ${pago['pago']}`;
  }
  catch(ex){
    throw ex;
  }
};

getInfoUsuario(id)
  .then(msg => console.log(msg))
  .catch(err => console.log(err));

