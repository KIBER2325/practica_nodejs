const clientes = [
    {
    id: 1, nombre: 'Leonor'},
    {
    id: 2, nombre: 'Jacinto'},
    {
    id: 3, nombre: 'Waldo'}
]
 const pagos = [
    {
        id: 1, pago: 1000, moneda: 'Bs'},
    {
        id: 2, pago: '1800', moneda: 'Bs'}
]

const id = 1


const getCliente=(id) =>{
    
    return new Promise((resolve,reject)=>{
    const cliente = clientes.find(e => e.id===id);
    if (cliente) {
        resolve(cliente);
    }else{
        reject(`No existe el cliente ${ id }`)
    }
    }) 
}


const getPago =(id) =>{
    return new Promise((resolve,reject)=>{
    const pago = pagos.find(s => s.id ===id);
    if (pago) {
        resolve(pago);
    }else{
        reject(`No existe el pago ${ id }`)
    }

    });
}




getCliente(id)
.then(cliente => console.log(cliente))
.catch(error => {
  //Codigo a realizar cuando se rechaza la promesa
  console.log(error);
});

getPago(id)
.then(pago => console.log(pago))
.catch(error => {
  //Codigo a realizar cuando se rechaza la promesa
  console.log(error);
});






  
    
  let nombre;

  

getCliente(id)
  .then(cliente => {
    console.log(cliente);
    nombre = cliente.nombre;
    //Siempre colocar un return para poder encadenar un then
    return getPago(id);
  })
  .then(pago => {
    console.log('El cliente:', nombre, 'tiene un pago de:', pago['pago']);
  })
  .catch(error => {
    //Codigo a realizar cuando se rechaza la promesa
    console.log(error);
  });




