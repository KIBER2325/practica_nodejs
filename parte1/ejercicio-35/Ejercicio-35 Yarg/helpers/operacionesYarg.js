
require('colors')
const fs = require('fs');

const crearSuma = (base = 5)=> { 

    let salidaS = '';
    titulo= `==================\nTabla del ${base}\n==================`.rainbow
    subtitulo = `\n==================\n`
    console.log(titulo);

    for(let i = 1; i<=10; i++){
      salidaS += `${base} + ${i} = ${base + i}\n`;
    }

    console.log(`${subtitulo}Suma${subtitulo}${salidaS}`);

    fs.writeFile(`salida-operaciones/Tabla-suma-${base}.txt`, salidaS, (err) => {
      if(err) throw err;
      console.log(`Tabla-suma-${base}.txt creado`.magenta);
    });
};



const crearResta = (base = 5)=> { 

  let salidaR = '';
  titulo= `==================\nTabla del ${base}\n==================`.rainbow
  subtitulo = `\n==================\n`
  console.log(titulo);

  for(let i = 1; i<=10; i++){
    salidaR += `${base} - ${i} = ${base- i}\n`;
  }

  console.log(`${subtitulo}Resta${subtitulo}${salidaR}`);

  fs.writeFile(`salida-operaciones/Tabla-resta-${base}.txt`, salidaR, (err) => {
    if(err) throw err;
    console.log(`Tabla-resta-${base}.txt creado`.magenta);
  });
};



const crearMulti = (base = 5)=> { 

  let salidaM = '';
  titulo= `==================\nTabla del ${base}\n==================`.rainbow
  subtitulo = `\n==================\n`
  console.log(titulo);

  for(let i = 1; i<=10; i++){
    salidaM += `${base} x ${i} = ${base* i}\n`;
  }

  console.log(`${subtitulo}Resta${subtitulo}${salidaM}`);

  fs.writeFile(`salida-operaciones/Tabla-multiplicacion-${base}.txt`, salidaM, (err) => {
    if(err) throw err;
    console.log(`Tabla-multiplicacion-${base}.txt creado`.magenta);
  });
};


const crearDivision = (base = 5)=> { 

  let salidaD = '';
  titulo= `==================\nTabla del ${base}\n==================`.rainbow
  subtitulo = `\n==================\n`
  console.log(titulo);

  for(let i = 1; i<=10; i++){
    salidaD += `${base} / ${i} = ${base/ i}\n`;
  }

  console.log(`${subtitulo}Resta${subtitulo}${salidaD}`);

  fs.writeFile(`salida-operaciones/Tabla-division-${base}.txt`, salidaD, (err) => {
    if(err) throw err;
    console.log(`Tabla-division-${base}.txt creado`.magenta);
  });
};



//exportacion del archivo
module.exports = {
  generarArchivos: crearSuma, 
  generarResta: crearResta,
  generarMulti: crearMulti,
  generarDivision: crearDivision
};
