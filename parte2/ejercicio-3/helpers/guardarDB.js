const fs =require('fs');

const archivo ='./db/data.json';
const archivoProd ='./db/dataProducto.json';

const guardarDBPersona = (data) => {
  // console.log(data);
  fs.writeFileSync(archivo ,JSON.stringify(data));
}

const getDBPersona = () =>{
  if (!fs.existsSync(archivo)) {
    return null;
  }

  //leer el archivo :
  fs
  const info = fs.readFileSync(archivo, {encoding: 'utf-8'});

  const data  = JSON.parse(info);
  // console.log(data);
  return data;
}


const guardarDBProducto = (data) => {
  // console.log(data);
  fs.writeFileSync(archivoProd ,JSON.stringify(data));
}

const getDBProducto = () =>{
  if (!fs.existsSync(archivoProd)) {
    return null;
  }

  //leer el archivo :
  fs
  const info = fs.readFileSync(archivoProd, {encoding: 'utf-8'});

  const data  = JSON.parse(info);
  // console.log(data);
  return data;
}



module.exports ={
  guardarDBPersona,
  getDBPersona,
  guardarDBProducto,
  getDBProducto
}