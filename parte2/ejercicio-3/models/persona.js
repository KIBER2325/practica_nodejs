const { v4: uuidv4 } = require('uuid');

class Persona {
  
  id = '';
  nombres = '';
  apellidos = '';
  ci = 0;
  id_Producto ='';

  constructor(nombres, apellidos, ci, idProduc) {
    this.id = uuidv4();
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.ci = ci;
    this.id_Producto =idProduc;
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Persona;