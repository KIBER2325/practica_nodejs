const Persona = require("./persona");

class Personas {

   constructor() {
       this._listado = [];
   }

    getPersona(id){
       return this._listado[id];
    }


    get listadoArr(){
       const listado  =[];
       //Me devuelve un arreglo de todas las keys
       Object.keys(this._listado).forEach(key =>{
       const persona = this._listado[key];
       listado.push(persona);
    })
       console.log(listado);
       return listado;
    }
 
  
    crearPersona(persona = {}) { 
      this._listado[persona.id] = persona;
    }

    cargarPersonasEnArray(personas=[]){
     //console.log(personas);
     personas.forEach(persona =>{
     this._listado[persona.id]=persona;
    })

   //console.log(this._listado);
  }



  borrarPersona(id ){
    delete this._listado[id];
  }

  buscarPorNombreOapellido(Variable){
    let persona;
    persona = this.listadoArr.find(e => e.nombres == Variable);
    if (persona) {
      return persona
    }else{
      persona = this.listadoArr.find(e =>e.apellidos ==Variable);
      if (persona) {
        return persona;
      }else{
        return  `El apellido o nombre "${Variable}" no existe`;
      }
    }

  }
 
 

  
}

module.exports = Personas;