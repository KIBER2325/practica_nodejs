const Persona = require("./persona");

class Productos {

    constructor() {
    this._listado = [];
    }

  get listadoArr(){
    const listado  =[];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key =>{
      //console.log(key);
      const producto = this._listado[key];
      //console.log(producto);
      listado.push(producto);
    })
 
      return listado;
  }
 
    crearProducto(producto = {}) {
    this._listado[producto.Id] = producto;
    }

     cargarProductosEnArray(productos=[]){
     productos.forEach(producto =>{
     this._listado[producto.Id]=producto;
     })
    }

    borrarProducto(id ){
    delete this._listado[id];
    }

    
    buscarPorNumeroOSerie(params){
       
      let producto;
      producto = this.listadoArr.find(e => e.Numero_serie == params);
  
      if (producto) {
      return producto;
      } else {
      producto = this.listadoArr.find(e => e.Fecha_caducidad == params);
    
       if (producto) {
        return producto;
      } else {
        return 'no existe el valor que ingreso';
      }  
     }
    }
 
    getProducto(id){
      return this._listado[id];
    }
  
  
}

module.exports = Productos;