const { guardarDBPersona, getDBPersona } = require('../helpers/guardarDB');
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const {request ,  response } = 'express';

const personas = new Personas();

const personasGet = (req, res = response) => {
  const personasDB =  getDBPersona();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  res.json({
    personasDB
  });
  // let array =   personas._listado;
};

const personasGetByNameOrLastName = (req, res = response) => {
    const {parametro} = req.params
    const personasDB =  getDBPersona();
    
    if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
    }
    
    let persona;
    if (parametro) {
      persona = personas.buscarPorNombreOapellido(parametro);
      res.json({
      persona
      });
    }else{
       res.json({
       msg: 'El dato ingresado no existe dentro de la DB'
    });
  }
  
  // let array =   personas._listado;
};





const personaPost = (req, res = response) => {
 const { nombres, apellidos, ci, id_prod } = req.body;
  const persona = new Persona(nombres, apellidos, ci, id_prod);
  let personasDB =  getDBPersona();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  personas.crearPersona(persona);
  // console.log(listado);
  guardarDBPersona(personas.listadoArr);
  personasDB =  getDBPersona();
  res.json({
    msg: 'post API - Controlador',
    personasDB
  });
};

const personaPut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    personas.borrarPersona(id);
    const { nombres, apellidos, ci, id_prod} = req.body;
    const persona = new Persona(nombres, apellidos, ci,id_prod);
    persona.setID(id);
    personas.crearPersona(persona);
    guardarDBPersona(personas.listadoArr);
  }
  res.json({
    msg: 'put API - Controlador'
  });
};

//Crear y mandar informacion  con POST:


const personaDelete = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    personas.borrarPersona(id);
    guardarDBPersona(personas.listadoArr);
  }
  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  personasGet,
  personasGetByNameOrLastName,
  personaPut,
  personaPost,
  personaDelete
}