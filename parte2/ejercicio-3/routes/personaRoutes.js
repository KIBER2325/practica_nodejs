const { Router } = require('express');
const { personasGet, personaPut, personaPost, personaDelete, personasGetByNameOrLastName } = require('../controllers/personaController');

const router = Router();

router.get('/', personasGet);

router.get('/:Variable', personasGetByNameOrLastName);

router.put('/:id', personaPut);

router.post('/', personaPost);

router.delete('/:id', personaDelete);

module.exports = router;