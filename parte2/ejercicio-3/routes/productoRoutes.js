const { Router } = require('express');
const { productosGet, productoPut, productoPost, productoDelete, getProductoPorSerieFecha, RelacionProductoPersona } = require('../controllers/productoControllers');

const router = Router();

router.get('/', productosGet);
router.get('/:parametroProducto', getProductoPorSerieFecha);
router.get('/:id/persona', RelacionProductoPersona);

router.put('/:id', productoPut);

router.post('/', productoPost);

router.delete('/:id', productoDelete);

module.exports = router;