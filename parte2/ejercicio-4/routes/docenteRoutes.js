const { Router } = require('express');
const { docentesGet, docentePut, docentePost, docenteDelete } = require('../controllers/docenteControllers');

const router = Router();

router.get('/', docentesGet);

router.put('/:id', docentePut);

router.post('/', docentePost);

router.delete('/:id', docenteDelete);

module.exports = router;