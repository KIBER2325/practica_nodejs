const { Router } = require('express');
const { examenesGet, examenPut, examenPost, examenDelete } = require('../controllers/examenControllers');

const router = Router();

router.get('/', examenesGet);

router.put('/:id', examenPut);

router.post('/', examenPost);

router.delete('/:id', examenDelete);

module.exports = router;