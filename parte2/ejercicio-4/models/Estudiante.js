const { v4: uuidv4 } = require('uuid');

class Estudiante {
  
  idEstudiante  = '';
  nombre   = '';
  apellidos = '';
  ci        = 0 ;
  materia   = '';
  Id_examen = '';

  constructor(nombre, apellidos, ci,materia,id_exam) {
    this.idEstudiante  = uuidv4();
    this.nombre   = nombre;
    this.apellidos = apellidos;
    this.ci        = ci;
    this.materia   = materia;
    this.Id_examen = id_exam;
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Estudiante;