const { v4: uuidv4 } = require('uuid');

class Examen{

  id             = '';
  Materia        = '';
  Fecha          = '';
  Nota           = 0; 
  Id_docente     = '';
  Id_Estudiante  = '';
  
  constructor(materia, fecha, nota, id_doc, id_est){
    this.id            = uuidv4();
    this.Materia       = materia;
    this.Fecha         = fecha;
    this.Nota          = nota;
    this.Id_docente    = id_doc;
    this.Id_Estudiante = id_est
  }

}

module.exports = Examen;
