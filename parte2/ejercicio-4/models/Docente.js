const { v4: uuidv4 } = require('uuid');

class Docente {
  
  id         = '';
  nombre    = '';
  apellidos  = '';
  materia  = '';
  Id_examen  = '';

  constructor(nombre, apellidos, materia, Id_examen) {
    this.id         = uuidv4();
    this.nombre    = nombre;
    this.apellidos  = apellidos;
    this.materia = materia;
    this.Id_examen  = Id_examen;
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Docente;