
class Examenes {

  constructor() {
    this._listado = [];
  }

  get listadoArr(){
    const listado  =[];
    Object.keys(this._listado).forEach(key =>{
      const examen = this._listado[key];
      listado.push(examen);
    })
 
    return listado;
  }
 
  crearExamen(examen = {}) {
    this._listado[examen.Id] = examen;
  }

  cargarExamenesEnArray(examenes=[]){
   examenes.forEach(examen =>{
     this._listado.push[examen.Id]=examen;
   })
  }

  borrarExamen(id ){
    delete this._listado[id];
  }

  getExamen(id){
    console.log(this._listado[id]);
    return this._listado[id];
  }
  
  
}

module.exports = Examenes;