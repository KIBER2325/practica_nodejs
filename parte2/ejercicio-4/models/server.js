const express = require('express');

class Server {

  constructor(){
    this.app = express();
    this.port = process.env.PORT;

    this.examenPath = '/api/examen';
    this.estudiantePath = '/api/estudiante';
    this.docentePath = '/api/docente';
    this.middlewares();

    this.routes();
  }

  middlewares() {    
    this.app.use(express.json());
    this.app.use(express.static('public'));
  }

  routes() {
     this.app.use(this.examenPath, require('../routes/examenRoutes'));
     this.app.use(this.estudiantePath, require('../routes/estudianteRoutes'));
     this.app.use(this.docentePath, require('../routes/docenteRoutes'));
  }

  listen() {
    //Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
    this.app.listen(this.port, () => {
    console.log('Servidor corriendo en puerto ', this.port);
    });
  }

}

module.exports = Server;