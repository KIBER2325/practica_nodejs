
class Docentes {

  constructor() {
    this._listado = [];
  }
  
  getdocente(id){
    return this._listado[id];
  }


  get listadoArr(){
    const listado  =[];
    Object.keys(this._listado).forEach(key =>{
      const docente = this._listado[key];
      listado.push(docente);
    })
    console.log(listado);
    return listado;
  }
 
  crearDocente(docente = {}) {
    this._listado[docente.id] = docente;
  }

  cargarDocentesEnArray(docentes=[]){
   //console.log(docentes);
   docentes.forEach(docente =>{
     this._listado[docente.id]=docente;
   })

   //console.log(this._listado);
  }

  borrarDocente(id ){
    delete this._listado[id];
  }

  
 
}

module.exports = Docentes;