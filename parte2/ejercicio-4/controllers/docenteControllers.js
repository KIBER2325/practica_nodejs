const { guardarDocente, obtDocente } = require('../helpers/guardarDB');
const {request ,  response } = 'express';

const Docente = require("../models/Docente");
const Docentes = require("../models/docentes");


const docentes = new Docentes();

const docentesGet = (req, res = response) => {
  const docentesDB =  obtDocente();
  if (docentesDB) {
    docentes.cargardocentesEnArray(docentesDB);
  }
  res.json({
    docentesDB
  });
};


const docentePost = (req, res = response) => {
  const { nombre, apellidos, materia, Id_examen } = req.body;
  const docente = new Docente( nombre, apellidos, materia, Id_examen);
  let docentesDB =  obtDocente();
  if (docentesDB) {
    docentes.cargarDocentesEnArray(docentesDB);
  }
  docentes.crearDocente(docente);
  guardarDocente(docentes.listadoArr);
  docentesDB =  obtDocente();
  res.json({
    docentesDB
  });
};

const docentePut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    docentes.borrarDocente(id);
    const { nombre, apellidos, materia, Id_examen} = req.body;
    const docente = new Docente(nombre, apellidos, materia, Id_examen);
    docente.setID(id);
    docentes.crearDocente(docente);
    guardarDocente(docentes.listadoArr);
  }
  res.json({
    msg: 'Actualizado correctamente'
  });
};



const docenteDelete = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    docentes.borrarDocente(id);
    guardarDocente(docentes.listadoArr);
  }
  res.json({
    msg: 'Eliminado con exito'
  });
};

module.exports = {
  docentesGet,
  docentePut,
  docentePost,
  docenteDelete
}