
const Estudiante = require("../models/estudiante");
const Estudiantes = require("../models/estudiantes");
const { guardarEstudiante, obtEstudiante } = require('../helpers/guardarDB');

const {request ,  response } = 'express';

const estudiantes = new Estudiantes();

const estudiantesGet = (req, res = response) => {
  const estudiantesDB =  obtEstudiante();
  if (estudiantesDB) {
    estudiantes.cargarEstudiantesEnArray(estudiantesDB);
  }
  res.json({
    estudiantesDB
  });
};


const estudiantePost = (req, res = response) => {
 //Actualizando lista de Personas
  let estudiantesDB =  obtEstudiante();
  if (estudiantesDB) {
    estudiantes.cargarEstudiantesEnArray(estudiantesDB);
  }
  const {nombre, apellidos, ci,materia,Id_examen} = req.body;
  const estudiante = new Estudiante(nombre, apellidos, ci, materia, Id_examen);
  estudiantes.crearEstudiante(estudiante);
  // console.log(listado);
  guardarEstudiante(estudiantes.listadoArr);
  estudiantesDB =  obtEstudiante();
  res.json({
    estudiantesDB
  });
};

const estudiantePut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    estudiantes.borrarEstudiante(id);
    const { nombre, apellidos, ci,materia,Id_examen} = req.body;
    const estudiante = new Estudiante(nombre, apellidos, ci,materia,Id_examen);
    estudiante.setID(id);
    estudiantes.crearEstudiante(estudiante);
    guardarEstudiante(estudiantes.listadoArr);
  }
  res.json({
    msg: 'Se actualizo exitosamente'
  });
};


const estudianteDelete = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    estudiantes.borrarEstudiante(id);
    guardarEstudiante(estudiantes.listadoArr);
  }
  res.json({
    msg: 'Se elimino correctamente'
  });
};

module.exports = {
  estudiantesGet,
  estudiantePut,
  estudiantePost,
  estudianteDelete
}