const { guardarExamen, obtExamen } = require('../helpers/guardarDB');
const Examen   = require("../models/Examen");
const Examenes = require("../models/examenes");

const {request ,  response } = 'express';


const examenes = new Examenes();

 
    //get examenes
    const examenesGet = (req, res = response) => {
      const examenesDB =  obtExamen();
      if (examenesDB) {
          examenes.cargarExamenesEnArray(examenesDB);
      }
        res.json({
        examenesDB
      });
    };


    const examenPost = (req, res = response) => {

    const { Materia, Fecha, Nota, Id_docente, Id_Estudiante } = req.body;
      const examen = new Examen( Materia , Fecha, Nota, Id_docente, Id_Estudiante);
      let examenesDB =  obtExamen();
      if (examenesDB) {
        examenes.cargarExamenesEnArray(examenesDB);
      }
      examenes.crearExamen(examen);
      
      console.log(examenes._listado);
      guardarExamen(examenes.listadoArr);
      examenesDB =  obtExamen();
      res.json({
        examenesDB
      });
    };

const examenPut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    examenes.borrarExamen(id);
    const {  Materia , Fecha, Nota, Id_docente,Id_Estudiante} = req.body;
    const examen = new Examen( Materia , Fecha, Nota, Id_docente,Id_Estudiante);
    examen.setID(id);
    examenes.crearExamen(examen);
    guardarExamen(examenes.listadoArr);
  }
  res.json({
    msg: 'Actualizado correctamente el examen'
  });
};


  const examenDelete = (req, res = response) => {
       const {id} = req.params;
       if (id) {
       examenes.borrarExamen(id);
       guardarExamen(examenes.listadoArr);
       }
       res.json({
       msg: 'Eliminado correctamente'
  });
};

module.exports = {
  examenesGet,
  examenPut,
  examenPost,
  examenDelete
}