
const { guardarDB, getDB } = require('../helpers/guardarDB');
const Persona = require("../models/persona");
const Personas = require("../models/personas");
const {request ,  response } = 'express';

//personas 
const personas = new Personas();
  

// GET normal 

const personasGet = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
if (personasDB) {
  res.json({
    personasDB
    });
}else{
      res.json({
        msg : "no existe ninguna persona"
      });
   }
};

//Post de una persona
const personaPost = (req, res = response) => {
  
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
  let personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  personas.crearPersona(persona);
  // console.log(listado);
  guardarDB(personas.listadoArr);
  personasDB =  getDB();
  res.json({
    msg: 'post API - Controlador',
    personasDB
  });
};



//actualizar Persona
const personaPut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    personas.borrarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
    persona.setID(id);
    personas.crearPersona(persona);
    guardarDB(personas.listadoArr);
  }
  res.json({
    msg: 'put API - Controlador'
  });
};



//borrar Personas
const personaDelete = (req, res = response) => {
    const {id} = req.params;
    // console.log(id);
    if (id) {
        personas.borrarPersona(id);
        guardarDB(personas.listadoArr);
     //console.log(personas._listado);
    }
      res.json({
      msg: 'delete API - Controlador'
      });
};  

module.exports = {
  personasGet,
  
  personaPut,
  personaPost,
  personaDelete
}