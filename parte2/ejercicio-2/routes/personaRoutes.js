const { Router } = require('express');
const { personasGet, personaPut, personaPost, personaDelete, personasGetByCIySexo, personasGetByLastNameAndGener} = require('../controllers/personaController');

const router = Router();

router.get('/', personasGet);

router.get('/:X',personasGetByCIySexo)

router.get('/:x/:sexo', personasGetByLastNameAndGener);

router.put('/:id', personaPut);

router.post('/', personaPost);

router.delete('/:id', personaDelete);

module.exports = router;