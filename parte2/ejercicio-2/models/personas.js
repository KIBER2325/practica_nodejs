const Persona = require("./persona");

class Personas {

  constructor() {
    this._listado = [];
  }

  get listadoArr(){
    const listado  =[];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key =>{
      //console.log(key);
      const persona = this._listado[key];
      //console.log(persona);
      listado.push(persona);
    })
 
    return listado;
  }


 
  crearPersona(persona = {}) {
    // this._listado.push(persona);
    /// return this._listado;
    this._listado[persona.id] = persona;
  }

  cargarPersonasEnArray(personas=[]){
   //console.log(personas);
   personas.forEach(persona =>{
     this._listado[persona.id]=persona;
   })

   //console.log(this._listado);
  }

  borrarPersona(id ){
    console.log(this._listado);
    delete this._listado[id];
    console.log(this._listado);

  }

  buscarPersona(nombres, apellidos){
    let persona = [];
     persona = this.listadoArr.filter(element => element.nombres == nombres && element.apellidos == apellidos);
    return persona;
  }


  buscarPersonaPorCI(ci){
    console.log("hola");
    let personaPorCI = [];
    personaPorCI = this.listadoArr.filter(e => e.ci == ci);
    return personaPorCI;
    
  }

  buscarPersonaPorsexo(sexo){
    console.log("hola");
    let personaPorSexo = [];
    personaPorSexo = this.listadoArr.filter(e => e.sexo == sexo);
    return personaPorSexo;
    
  }

 
  obtenerFiltro(s, sex){
    let personasXD;
    console.log(s,sex);
    personasXD = this.listadoArr.filter(e => e.apellidos.includes(s) && e.sexo == sex);
    
    console.log(personasXD);
    return personasXD;
  }

  
}

module.exports = Personas;