const { guardarDB, getDB } = require('../helpers/guardarDB');
const Persona = require("../models/persona");
const Personas = require("../models/personas");
const {request ,  response } = 'express';

//personas 
const personas = new Personas();
  

// GET normal y por nombre y apellido

const personasGet = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }

   const {nombres, apellidos} = req.query
   console.log(nombres,apellidos);
   let persona ;
   if (nombres && apellidos){
      persona = personas.buscarPersona(nombres,apellidos);
      res.json({
      persona
      });
      
   }else{
      res.json({
        personasDB
      });
   }
};


//get pot ci y sexo

const personasGetByCIySexo = (req, res = response) => {
   const personasDB =  getDB();
     if (personasDB) {
        personas.cargarPersonasEnArray(personasDB);
     }
   
        const {X} =req.params;
        let CIPersona;
        let personaSexo;
  
     if (parseInt(X)){
          CIPersona = personas.buscarPersonaPorCI(X);
          res.json({
          CIPersona
        });
      }else{
           personaSexo = personas.buscarPersonaPorsexo(X)
           res.json({
           personaSexo
      });
    }
 
}
 

//get por  letra x y sexo
 const personasGetByLastNameAndGener = (req, res = response) => {
   const personasDB =  getDB();
   if (personasDB) {
     personas.cargarPersonasEnArray(personasDB);
   }

   const  {x, sexo} = req.params;
   
   
   let personass ;
   if (x && sexo) {
    
     personass = personas.obtenerFiltro(x, sexo); 
    
     if (personass !== []) {
     
       res.json({
         msg: 'post API - Controlador',
         personass
        });
       }else{
       res.json({
         msg: 'No existen los datos',
       });
       }
   }
 }; 





//Post de una persona
const personaPost = (req, res = response) => {
  
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
  let personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  personas.crearPersona(persona);
  // console.log(listado);
  guardarDB(personas.listadoArr);
  personasDB =  getDB();
  res.json({
    msg: 'post API - Controlador',
    personasDB
  });
};



//actualizar Persona
const personaPut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    personas.borrarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
    persona.setID(id);
    personas.crearPersona(persona);
    guardarDB(personas.listadoArr);
  }
  res.json({
    msg: 'put API - Controlador'
  });
};



//borrar Personas
const personaDelete = (req, res = response) => {
    const {id} = req.params;
    // console.log(id);
    if (id) {
        personas.borrarPersona(id);
        guardarDB(personas.listadoArr);
     //console.log(personas._listado);
    }
      res.json({
      msg: 'delete API - Controlador'
      });
};  

module.exports = {
  personasGet,
  personasGetByCIySexo,
  personasGetByLastNameAndGener,
  personaPut,
  personaPost,
  personaDelete
}