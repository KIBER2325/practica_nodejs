//kiber Mena Copa

const { request ,  response } = 'express';

const meses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto', 'septiembre','octubre', 'nomviembre','diciembre'];
const  dias = ['domingo','lunes','martes','miércoles','jueves','viernes','sábado'];


const cero = (num) => {
    console.log(num);
return num < 10 ? '0'+num : num;

}

const ObtenerFecha = (req, res = response) => {
    const date = new Date();
    res.json({
       
        day  : ` ${dias[date.getDay()]} `,

        date_1 : `${date.getDate()} De ${meses[date.getMonth()]}`,

        year   : date.getFullYear(),
        
        date_2 : `${cero(date.getDate())}/${cero(date.getMonth()+1)}/${date.getFullYear()}`,

        hour   : `${cero(date.getHours())}:${cero(date.getMinutes())}:${cero(date.getSeconds())}`,
        prefix : `${date.getHours() > 11? 'PM': 'AM'}`
        
    })

};
module.exports = {
    ObtenerFecha
  };
